FROM openjdk:8-jdk

RUN apt-get --quiet update --yes

RUN apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1 nodejs npm

RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip

RUN unzip -d android-sdk-linux android-sdk.zip

RUN echo y | android-sdk-linux/tools/bin/sdkmanager "platforms;android-28" >/dev/null

RUN echo y | android-sdk-linux/tools/bin/sdkmanager "platform-tools" >/dev/null

RUN echo y | android-sdk-linux/tools/bin/sdkmanager "build-tools;29.0.0" >/dev/null

ENV ANDROID_HOME=$PWD/android-sdk-linux

RUN echo $ANDROID_HOME

RUN echo $PATH

ENV PATH=$PATH:$PWD/android-sdk-linux/platform-tools/

RUN echo $PATH

RUN yes | android-sdk-linux/tools/bin/sdkmanager --licenses

RUN rm android-sdk.zip


